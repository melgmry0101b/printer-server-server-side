<?php
/*
** PrintJobOrder.php
** Order Structure for printing
**
** AUTHOR  : Mohammed Elghamry <elghmary_work@outlook.com>
** CREATED : 26/10/2018 08:04 PM
*/

namespace PrinterServer;

class PrintJobOrder
{
    /** @var string $OrderID Order ID From the system */
    public $OrderID;
    /** @var string $OrderType Order Type e.g. Local or Takeaway  */
    public $OrderType;
    /** @var int $Timestamp Time the order created in */
    public $Timestamp;

    /** @var string $CashierName */
    public $CashierName;
    /** @var string $Table -Leave as "" if not applicable */
    public $Table;

    /** @var float $TotalPrice Total price of the order */
    public $TotalPrice;
    
    /** @var float $TotalDiscount Total discount on whole order */
    public $TotalDiscount;
    
    /** @var float $Tax Value of the tax */
    public $Tax;

    /** @var float $NetTotal Total after discount and tax */
    public $NetTotal;

    /** @var float $PaidCash Cash Paid by the customer */
    public $PaidCash;
    /** @var float $PaidVisa Visa Paid by the customer */
    public $PaidVisa;
    
    /** @var float $ChangeDue Remaining of what the customer paid = NetTotal - (Paid Cash + Paid Visa + Allowance) */
    public $ChangeDue;
}
