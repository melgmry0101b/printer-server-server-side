<?php
/*
** ErrorLog.php
** Simple Logging Support
**
** AUTHOR  : Mohammed Elghamry <elghmary_work@outlook.com>
** CREATED : 26/10/2018 06:15 AM
*/

namespace PrinterServer;

class ErrorLog
{
    const ERRORLOG_PATH = __DIR__ . '/log/error.log';

    // Our class is static, no instantiation
    private function __construct()
    {
    }

    public static function log($errorstr)
    {
        return error_log(
            "[" . date(DATE_ATOM) . "] : " . $errorstr . "\n",
            3,
            self::ERRORLOG_PATH
        );
    }
}
