<?php
/*
** PrintJob.php
** Print Job For Printer Server
**
** AUTHOR  : Mohammed Elghamry <elghmary_work@outlook.com>
** CREATED : 26/10/2018 07:50
*/

namespace PrinterServer;

class PrintJob
{
    /**
     * Job Token
     *
     * It is Set using reflection
     *
     * @var string $JobToken
     */
    private $JobToken;

    /**
     * Job Timestamp
     *
     * @var int $CreatedTimestamp
     */
    private $CreatedTimestamp;

    /**
     * Type of the print job
     *
     * @var string $JobType
     */
    public $JobType;
    
    /**
     * if this print job should be printed as canceled
     *
     * @var bool $PrintAsCanceled
     */
    public $PrintAsCanceled;

    /**
     * Order Details
     *
     * @var \PrinterServer\PrintJobOrder $OrderDetails
     */
    public $OrderDetails;

    /**
     * Order Food Items
     *
     * @var \PrinterServer\PrintJobFoodItem[] $OrderFoodItem
     */
    public $OrderFoodItems;

    /**
     * Creates new job
     */
    public function __construct()
    {
        $this->CreatedTimestamp = time();
    }

    /**
     * Gets job token
     *
     * @return string
     */
    public function getToken()
    {
        return $this->JobToken;
    }

    /**
     * Serialize
     *
     * @return string
     */
    public function serialize()
    {
        // Primary Details
        $arrayToSerialize = array(
            'JobToken'          => $this->JobToken,
            'CreatedTimestamp'  => $this->CreatedTimestamp,
            'JobType'           => $this->JobType,
            'PrintAsCanceled'   => $this->PrintAsCanceled,
            'OrderDetails'      =>
                is_a($this->OrderDetails, 'PrinterServer\PrintJobOrder')
                ? get_object_vars($this->OrderDetails)
                : null,
            'OrderFoodItems'    => array()
        );

        // Convert Food Items to assoc-array
        if (is_array($this->OrderFoodItems)) {
            foreach ($this->OrderFoodItems as $OrderFoodItem) {
                if (is_a($OrderFoodItem, 'PrinterServer\PrintJobFoodItem')) {
                    array_push(
                        $arrayToSerialize['OrderFoodItems'],
                        get_object_vars($OrderFoodItem)
                    );
                }
            }
        }

        return serialize($arrayToSerialize);
    }
}
