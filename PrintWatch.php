<?php
/*
** PrintWatch.php
** Watch Printing Jobs through SSE
**
** AUTHOR  : Mohammed Elghamry <elghmary_work@outlook.com>
** CREATED : 27/10/2018 01:30 AM
*/

require_once "Print.php";

// Set script time limit to infinity
set_time_limit(0);

define('KEEPALIVE_INTERVAL', 75); // 1.25 Minutes

// Set Headers
header("Content-Type: text/event-stream; charset=utf-8");
header("Cache-Control: no-cache");

// Send Connection Accepted
sendEvent("ConnectionAccepted", time());

// Last sent keep alive
$lastKeepAliveTime = time();

while (true) {
    // Check every 10 seconds
    sleep(10);

    // Send Keep Alive
    if (time() - $lastKeepAliveTime >= KEEPALIVE_INTERVAL) {
        $lastKeepAliveTime = time();
        sendEvent("KeepAlive", $lastKeepAliveTime);
    }

    // Get All Orders
    $printJobsData = \PrinterServer\PrinterServer::getAllJobs();
    if (empty($printJobsData)) {
        continue;
    }

    // Data Found, Send them
    foreach ($printJobsData as $printJobData) {
        $printJob = unserialize($printJobData, array('allowed_classes' => false));
        $printJobJson = json_encode(
            $printJob,
            JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE | JSON_NUMERIC_CHECK
        );
        sendEvent("PrintJob", $printJobJson);
    }
}

/**
 * Send Event
 *
 * @param string $event
 * @param string $data
 * @return void
 */
function sendEvent(string $event, string $data)
{
    echo "event: " . $event . "\n";
    echo "data: " . $data . "\n\n";

    // Flush Buffer
    ob_flush();
    flush();
}
