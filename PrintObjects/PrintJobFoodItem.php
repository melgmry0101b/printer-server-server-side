<?php
/*
** PrintJobFoodItem.php
** Print Job Food Item
**
** AUTHOR  : Mohammed Elghamry <elghmary_work@outlook.com>
** CREATED : 26/10/2018 08:28 PM
*/

namespace PrinterServer;

class PrintJobFoodItem
{
    /** @var string $Category Food Item Category */
    public $Category;
    /** @var string $Chief Food Item Chief */
    public $Chief;

    /** @var string $NameEnglish */
    public $NameEnglish;
    /** @var string $NameArabic */
    public $NameArabic;

    /** @var string[] $Additions */
    public $Additions;

    /** @var int $Count Count of this item */
    public $Count;

    /** @var float $PricePerUnit Price per one unit of the item */
    public $PricePerUnit;
    /** @var float $TotalPrice Total Price of this item */
    public $TotalPrice;
}
