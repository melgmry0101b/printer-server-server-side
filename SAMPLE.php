<?php
// @codingStandardsIgnoreFile

require_once "Print.php";

$PrintJob = new \PrinterServer\PrintJob();
$PrintJob->JobType = 'Cashier'; // [Cashier, Waiter, Chief]
$PrintJob->PrintAsCanceled = false;

$PrintJob->OrderDetails = new \PrinterServer\PrintJobOrder();
$PrintJob->OrderDetails->Timestamp      = time();
$PrintJob->OrderDetails->OrderID        = "Order #10001";
$PrintJob->OrderDetails->CashierName    = 'Cashier 1';
$PrintJob->OrderDetails->OrderType      = "Local";
$PrintJob->OrderDetails->Table          = "Table 1";
$PrintJob->OrderDetails->TotalPrice     = 500;
$PrintJob->OrderDetails->TotalDiscount  = 50;
$PrintJob->OrderDetails->Tax            = 22.5;
$PrintJob->OrderDetails->NetTotal       = 472.5;
$PrintJob->OrderDetails->PaidCash       = 0;
$PrintJob->OrderDetails->PaidVisa       = 472.5;
$PrintJob->OrderDetails->ChangeDue      = 0;

$PrintJob->OrderFoodItems = array();

$foodItem = new \PrinterServer\PrintJobFoodItem();
$foodItem->Category         = "Grills";
$foodItem->NameEnglish      = "Grilled Chicken";
$foodItem->NameArabic       = "دجاج مشوي";
$foodItem->Chief            = "GrillsChief";
$foodItem->Additions        = array("Spicy", "More Sauce");
$foodItem->Count            = 5;
$foodItem->PricePerUnit     = 20;
$foodItem->TotalPrice       = 100;
$PrintJob->OrderFoodItems[] = $foodItem; // Append the food item

if (\PrinterServer\PrinterServer::addJobToQueue($PrintJob)) {
    echo "SUCCESS";
} else {
    echo "FAILED";
}
